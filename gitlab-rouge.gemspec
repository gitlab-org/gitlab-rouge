require './lib/rouge/version'

Gem::Specification.new do |s|
  s.name = "gitlab-rouge"
  s.version = Rouge.version
  s.authors = ["Jeanine Adkisson", "Douwe Maan"]
  s.email = ["jneen@jneen.net", "douwe@gitlab.com"]
  s.summary = "A pure-ruby colorizer based on pygments"
  s.description = <<-desc.strip.gsub(/\s+/, ' ')
    Rouge aims to a be a simple, easy-to-extend drop-in replacement
    for pygments.
  desc
  s.homepage = "https://gitlab.com/gitlab-org/gitlab-rouge"
  s.files = Dir['Gemfile', 'LICENSE', 'gitlab-rouge.gemspec', 'lib/**/*.rb', 'lib/**/*.yml', 'bin/rougify', 'lib/rouge/demos/*']
  s.executables = %w(rougify)
  s.license = 'MIT (see LICENSE file)'
end
